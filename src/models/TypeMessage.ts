import { Sequelize, DataTypes, Model } from 'sequelize';

export class TypeMessage extends Model {}

export const init = (sequelize: Sequelize) => {
    TypeMessage.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'ID'
        },
        type: {
            type: DataTypes.STRING,
            get() {
                const type = this.getDataValue('type');
                return type?.trim();
            },
            field: 'Type'
        },
    },
    {
        sequelize,
        modelName: 'TypeMessage',
        timestamps: false,
        tableName: 'TypeMessage'
    });
}