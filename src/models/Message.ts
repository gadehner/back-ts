import { Sequelize, DataTypes, Model } from 'sequelize';
import { TypeMessage } from './TypeMessage';

export class Message extends Model {}

export const init = (sequelize: Sequelize) => {
    Message.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'ID'
        },
        message: {
            type: DataTypes.STRING,
            get() {
                const message = this.getDataValue('message');
                return message?.trim();
            },
            field: 'Message'
        },
        nick: {
            type: DataTypes.STRING,
            get() {
                const nick = this.getDataValue('nick');
                return nick?.trim();
            },
            field: 'Nick'
        },
    },
    {
        sequelize,
        modelName: 'Message',
        timestamps: false,
        tableName: 'Message'
    });
}

export const configRelationship = () => {
    Message.belongsTo(TypeMessage, {
        foreignKey: 'TypeId'
    });
}