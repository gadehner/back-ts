import dotenv from 'dotenv';
import { setupSequalizeDbConnection } from './db/db';
import { startServer } from './server';

dotenv.config();
setupSequalizeDbConnection().then(()=>{
    startServer();
})