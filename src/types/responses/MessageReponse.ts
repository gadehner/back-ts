import { Message } from "../../models/Message";


export type MessageResponse = Message | null;

export type MessageAllResponse = MessageResponse[] ;
