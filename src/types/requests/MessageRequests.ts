export interface NewMessage {
    message: string,
    nick: string,
    TypeId: number,
}