import { validationResult } from 'express-validator';
import { Response, Request } from 'express';
import { StatusCodes } from 'http-status-codes';

export const validateResult = (
    req: Request,
    res: Response,
    next: () => void
) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const errorMessage = errors.array()[0].msg;
        // const errorResponse: ErrorMessageResponse = {
        //     errorCode: ErrorCode.INCORRECT_BODY_PARAMS,
        //     errorMessage,
        // };
        return res.status(StatusCodes.BAD_REQUEST).json(errorMessage);
    }
    next();
};
