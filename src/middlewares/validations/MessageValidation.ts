import { body, param } from 'express-validator';
// import { ValidationErrorMessages } from '../../types/responses/ErrorMessageResponse';
import { validateResult } from './util';

export const getOneMessageValidation = () => {
    return [
        param('id')
            .exists()
            .withMessage("The id is missing")
            .isNumeric()
            .withMessage("The id should be a number"),
        validateResult
    ]
}

export const newMessageValidation = () => {
    return [
        body('message')
            .exists()
            .withMessage("The message is missing")
            .isString()
            .withMessage("The message should be a string"),
        body('nick')
            .exists()
            .withMessage("The nick is missing")
            .isString()
            .withMessage("The nick should be a string"),
        body('TypeId')
            .exists()
            .withMessage("The TypeId is missing")
            .isNumeric()
            .withMessage("The TypeId should be a number"),
        validateResult
    ]
}

// export const invalidEmailMsg = (fieldName: string = "email") => `Invalid email format for field ${fieldName}`;

// export const missingFieldMsg = (fieldName: string) => `The ${fieldName} is missing`;