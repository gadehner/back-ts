import { Request, Response, NextFunction} from 'express';
// import {ErrorCode, ErrorMessageResponse} from '../types/responses/ErrorMessageResponse';
// import { ServiceError } from '../types/ServiceError';
import StatusCode from 'http-status-codes';

export const handleErrors = (err: any, req: Request, res: Response, next: NextFunction) => {

    if (err.constructor.name === 'ServiceError') {
        // const response: ErrorMessageResponse = {
        //     errorCode: err.getCode(),
        //     errorMessage: err.getMessage()
        // }
        res.status(err.getHttpStatus()).json(err.message);
    } else {
        // const response: ErrorMessageResponse = {
        //     errorCode: ErrorCode.GENERAL_ERROR,
        //     errorMessage: err.message
        // }
        res.status(StatusCode.INTERNAL_SERVER_ERROR).json(err.message);
    }
};
