import { Request, Response, NextFunction } from 'express';
import { Message } from '../models/Message';
import { TypeMessage } from '../models/TypeMessage';
import { NewMessage } from '../types/requests/MessageRequests';
import { MessageAllResponse, MessageResponse } from '../types/responses/MessageReponse';
import { SuccessResponse } from '../types/responses/SuccessResponse';

const getAll = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const messages = await Message.findAll();
        // throw new Error("Unexpected error");
        const response: MessageAllResponse = messages;
        res.json(response)
    } catch (e) {
        next(e)
    }
}

const getOne = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const message = await Message.findOne({
            where:{
                id: req.params.id
            },
            include:[
                {
                    model: TypeMessage
                }
            ]
        });
        const response: MessageResponse = message;
        res.json(response)
    } catch (e) {
        next(e)
    }
}

const store = async (req: Request, res: Response, next: NextFunction) => {
    const newMessage: NewMessage = req.body;
    try {
        const message = Message.build({
           ...newMessage
        });
        await message.save();
        const response: SuccessResponse = {
            success: true
        };
        res.json(response)
    } catch (e) {
        next(e)
    }
}

const update = async (req: Request, res: Response, next: NextFunction) => {
    const newMessage: NewMessage = req.body;
    try {
        await Message.update({
           message: newMessage.message,
           nick: newMessage.nick,
        }, {
            where: {
                id:req.params.id
            }            
        });
        const response: SuccessResponse = {
            success: true
        };
        res.json(response)
    } catch (e) {
        next(e)
    }
}

export default {
    getAll,
    getOne,
    store
}