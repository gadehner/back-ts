import express from 'express';
import MessageController from './controllers/MessageController';
import { handleErrors } from './middlewares/errors/handleErrors';
import { getOneMessageValidation, newMessageValidation } from './middlewares/validations/MessageValidation';

export const startServer = () => {

    const app = express();

    app.use(express.json());

    app.get('/messages', MessageController.getAll)

    app.get('/message/:id', ...getOneMessageValidation(), MessageController.getOne)

    app.post('/message', ...newMessageValidation(), MessageController.store)
    
    app.use(handleErrors);

    app.listen(process.env.PORT,() =>{
        console.log("Escuchando en puerto: " + process.env.PORT)
    })
}
