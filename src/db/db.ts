import { Sequelize } from 'sequelize';
import { init as initTypeMessage } from '../models/TypeMessage';
import { init as initMessage, configRelationship as configMessageRelationship } from '../models/Message';

const testConnection = async (sequelize: Sequelize) => {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully: '+process.env.DB_NAME);
      } catch (error) {
        console.log(error);
        console.error('Unable to connect to the database: '+process.env.DB_NAME_UILS, error);
      }
}

const initModels = (sequelize: Sequelize) => {
    initTypeMessage(sequelize);
    initMessage(sequelize);
}

const initRelationships = () => {
    configMessageRelationship();
}

export const setupSequalizeDbConnection = async (): Promise<Sequelize> => {
    const sequelize = new Sequelize(
        process.env.DB_NAME || '',
        process.env.DB_USER || 'sa',
        process.env.DB_PASS || '' ,
        {
            host: process.env.DB_SERVER || 'localhost',
            dialect: "mssql",
            port: Number(process.env.DB_PORT) || 1433,
            pool: {
                max: 5,
                min: 0,
                acquire: 30000,
                idle: 10000
            },
            dialectOptions: {
                "options": {
                    validateBulkLoadParameters: true
                }
            },
            define:{
                freezeTableName: true
            }
        }
    );
    await testConnection(sequelize);
    initModels(sequelize);
    initRelationships();
    return sequelize;
}