BEGIN TRANSACTION

-- CREATE TABLE "TypeMessage" ----------------------------------
CREATE TABLE [dbo].[TypeMessage] ( 
	[ID] INT IDENTITY ( 1, 1 )  NOT NULL, 
	[Type] NVARCHAR( 255 ) NOT NULL,
	PRIMARY KEY ( [ID] ) )
GO
-------------------------------------------------------------

COMMIT TRANSACTION


BEGIN TRANSACTION;

-- CREATE TABLE "Message" --------------------------------------
CREATE TABLE [dbo].[Message] ( 
	[ID] INT IDENTITY ( 1, 1 )  NOT NULL, 
	[Nick] NVARCHAR( 255 ) NOT NULL, 
	[Message] NVARCHAR( 255 ) NULL, 
	[TypeId] INT NULL,
	PRIMARY KEY ( [ID] ) )
GO;
-------------------------------------------------------------

COMMIT TRANSACTION;

BEGIN TRANSACTION;

-- CREATE LINK "FK_a35756ef1325c162740bde0212c" ----------------
ALTER TABLE [dbo].[Message]
	ADD CONSTRAINT [FK_a35756ef1325c162740bde0212c]
	FOREIGN KEY ([TypeId])
	REFERENCES [dbo].[TypeMessage]( [ID] )
	ON DELETE No Action
	ON UPDATE No Action
GO;
-----------------------------------------------------------

COMMIT TRANSACTION;
